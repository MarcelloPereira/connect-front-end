import { Component, OnInit } from '@angular/core';
import { DataModel } from './data.model';
import { HomeService } from './home.service';
import { PageModel } from './page.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  search = {
    page: 0,
    per_page: 15
  };

  code!: any;

  loading = false;

  pageData!: PageModel;
  data: DataModel[] | undefined;

  constructor(
      private homeService: HomeService,
      private toastr: ToastrService
    ) {
  }

  ngOnInit(): void {
  }

  loadData(): void {
    this.loading = true;
    this.homeService.getIndicators(this.code, this.search).subscribe((res) => {

      if (res[0].message) {
        this.toastr.error(res[0]?.message[0]?.value || 'Error');
      }

      this.pageData = res[0];
      this.data = res[1];
      this.loading = false;
    }, err => {
      this.toastr.error(err);
      this.loading = false;
    });
  }

  loadPage($event: any): void {
    this.search.page = $event;
    this.loadData();
  }

  submit(): void{
    this.loadData();
  }
}
