import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient) { }

  getIndicators(code: string, req: any ): Observable<any> {
    const params = {
      page: req.page || 1,
      per_page: req.per_page || 20
    };
    return this.http.get(environment.connecUrl + `/api/world-bank/${code}`, {
      params
    });
  }
}
